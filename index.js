// console.log('hello');

const getCube = Math.pow(2, 3);

let num = getCube;
let message = 'The cube of 2 is ' + num
console.log(message);

const fullAdd= ['258', 'Washington Ave.', 'NW', 'California', '90011'];
const [houseNo, street, suffixDirection, state, zipCode] = fullAdd;
const messageAdd = `I live at ${[houseNo]} ${[street]} ${[suffixDirection]}, ${[state]} ${[zipCode]}`;
console.log(messageAdd);
		
const animal = {
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	feet: 20,
	inches: 3
}
const {type, weight, feet, inches} = animal;
const messageAnimal = `Lolong was a ${[type]}. He weighed at ${[weight]} with a measurement of ${[feet]} ft ${[inches]} in.`;
console.log(messageAnimal);

const numbers = ['1', '2', '3', '4', '5'];

numbers.forEach((numbers)=> {
			console.log(numbers)
		}
	)

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

}

const myDog = new Dog ()

myDog.name = 'Frankie';
myDog.age = 5;
myDog.breed = 'Miniature Dachhund';

console.log(myDog);


